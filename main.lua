lg = love.graphics

wdow = {wth = 960, hgt = 540} --16/9 ratio

font = lg.newFont("consola.ttf", 13)
bigFont = lg.newFont("consola.ttf", 16)
hugeFont = lg.newFont("consola.ttf", 72)

counter = {0, 0}
container = {}
mouseX = 0
mouseY = 0
gravityForceSpeed = 0.00001
gravityForceStep = 0.00001
speedLossForce = 0
speedLossForceStep = 0.0001
pause = false
menu = false
fullscreen = false
options = {}
options.isAttractedByObject = true
options.isAttractedByMouse = false
options.isSpeedLimited = true
options.speedLimit = 10
options.screenBorder = 0
options.objectsMass = 4

help = {}
help.val = false
help.wth = 400
help.hgt = 200

function massToRad(mass)
  --2 mass = 1 rad  [1 mass = 1 pixel diametre]
  return mass/2
end

function createObject(id, x, y, mass)
  --[[
  ID
  0 : Objet avec une masse soumis à la gravité
  1 : Objet avec une masse non soumis à la gravité
  --]]
  if id == 0 then
    counter[1] = counter[1] + 1
  else
    counter[2] = counter[2] + 1
  end
  
  local obj = {}
  obj.id = id
  obj.x = x
  obj.y = y
  obj.spdX = 0
  obj.spdY = 0
  if not mass then
    obj.mass = math.random(3, 12) + math.random(1, 9)/10
  else
    obj.mass = mass
  end
  obj.rad = massToRad(obj.mass)
  if id == 0 then
    obj.drawType = "fill"
  elseif id == 1 then
    obj.drawType = "line"
  end
  obj.colors = {r = 255, g = 255, b = 200}
  table.insert(container, obj)
end

function overlapCircle(obj, obj2)
  if (obj.x + obj.rad > obj2.x - obj2.rad and obj.x - obj.rad < obj2.x + obj.rad) and
  (obj.y + obj.rad > obj2.y - obj2.rad and obj.y - obj.rad < obj2.y + obj.rad) then
    objCombined = obj
    objCombined.mass = obj.mass + obj2.mass
    objCombined.rad = massToRad(objCombined.mass)
    return objCombined
  else return false end
end

function getAttracted(obj, x, y, dt)
  obj.spdY = obj.spdY + obj.mass*(y - obj.y)*gravityForceSpeed*dt
  obj.spdX = obj.spdX + obj.mass*(x - obj.x)*gravityForceSpeed*dt
  
  return obj
end

function moveObject(obj, dt)
  --Fait avancer la fenêtre
  obj.x = obj.x + obj.spdX
  obj.y = obj.y + obj.spdY
  
  --[[
  ID
  0 : Change de bord de fenêtre
  1 : Rebondit sur la fenêtre
  2 : Ne fait rien en touchant un bord de fenêtre
  ]]--
  
  --Bords de la fenêtre
  if options.screenBorder == 0 then
    --Change de bord de fenêtre
    if obj.x + obj.rad < 0 then obj.x = wdow.wth - obj.rad; obj.spdX = 0 end
    if obj.x - obj.rad > wdow.wth then obj.x = obj.rad; obj.spdX = 0 end
    
    if obj.y - obj.rad < 0 then obj.y = wdow.hgt - obj.rad; obj.spdY = 0 end
    if obj.y + obj.rad > wdow.hgt then obj.y = obj.rad; obj.spdY = 0 end
  elseif options.screenBorder == 1 then
    --Rebondit sur la fenêtre
    if obj.x + obj.rad < 0 then obj.spdX = -obj.spdX end
    if obj.x - obj.rad > wdow.wth then obj.spdX = -obj.spdX end
    
    if obj.y - obj.rad < 0 then obj.spdY = -obj.spdY end
    if obj.y + obj.rad > wdow.hgt then obj.spdY = -obj.spdY end
  end
  
  return obj
end


function speedLoss(obj, dt)
  if obj.spdX > 0 then
    if obj.spdX - speedLossForce < 0 then
      obj.spdX = 0
    else
      obj.spdX = obj.spdX - speedLossForce
    end
  elseif obj.spdX < 0 then
    if obj.spdX + speedLossForce > 0 then
      obj.spdX = 0
    else
      obj.spdX = obj.spdX + speedLossForce
    end
  end    
  
  if obj.spdY > 0 then
    if obj.spdY - speedLossForce < 0 then
      obj.spdY = 0
    else
      obj.spdY = obj.spdY - speedLossForce
    end
  elseif obj.spdY < 0 then
    if obj.spdY + speedLossForce > 0 then
      obj.spdY = 0
    else
      obj.spdY = obj.spdY + speedLossForce
    end
  end
  
  return obj
end

function love.load()
  require "conf"
  
  math.randomseed(os.time())
end

function love.keypressed(key)
  if key == "tab" then menu = not menu end
  if key == "space" then help.val = not help.val end
  if key == "p" then pause = not pause end
  if key == "f11" then fullscreen = not fullscreen; love.window.setFullscreen(fullscreen) end
  --if key == "escape" then love.event.quit() end
  
  if key == "1" then options.isAttractedByObject = not options.isAttractedByObject end
  if key == "2" then options.isAttractedByMouse = not options.isAttractedByMouse end
  if key == "3" then options.isSpeedLimited = not options.isSpeedLimited end
  if key == "4" then 
    if options.screenBorder == 2 then
      options.screenBorder = 0
    else
      options.screenBorder = options.screenBorder + 1
    end
  end
  if key == "5" then 
    if options.objectsMass == 4 then
      options.objectsMass = 0
    else
      options.objectsMass = options.objectsMass + 1
    end
  end
  
  --Supprime un objet non soumis à la gravité
  if key == "delete" then
    for i = #container, 1, -1 do
      if container[i].id == 1 then
        counter[2] = counter[2] - 1
        table.remove(container, i)
        return
      end
    end
  end
  
  --Supprime un objet soumis à la gravité
  if key == "backspace" then
    for i = #container, 1, -1 do
      if container[i].id == 0 then
        counter[1] = counter[1] - 1
        table.remove(container, i)
        return
      end
    end
  end
end

function love.wheelmoved(x, y)
  gravityForceSpeed = tonumber(string.format("%.5f", gravityForceSpeed))
  speedLossForce = tonumber(string.format("%.4f", speedLossForce))
  
  if love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl") then
    if y > 0 then
      speedLossForce = speedLossForce + speedLossForceStep
    else
      speedLossForce = speedLossForce - speedLossForceStep
    end
  else
    if y > 0 then
      gravityForceSpeed = gravityForceSpeed + gravityForceStep
    else
      gravityForceSpeed = gravityForceSpeed - gravityForceStep
    end
  end
end

function love.mousepressed(x, y, button)
  local mass = 0
  if options.objectsMass == 0 then mass = 3
  elseif options.objectsMass == 1 then mass = 7
  elseif options.objectsMass == 2 then mass = 12
  elseif options.objectsMass == 3 then mass = 40
  elseif options.objectsMass == 4 then mass = math.random(3, 12)
  end
  
  if button == 1 then
    createObject(0, x, y, mass)
  end
  
  if button == 2 then
    createObject(1, x, y, mass)
  end
end

function love.update(dt)
  if dt < 0.2 then
    --Vérifie si l'on a changer la taille de la fenêtre
    wdow.wth, wdow.hgt = love.window.getMode()
    
    mouseX, mouseY = love.mouse.getPosition()
    
    if not pause then
      for i, v in pairs(container) do
        if v.id == 0 then
          if options.isAttractedByObject then
            for ii, vv in pairs(container) do
              v = getAttracted(v, vv.x, vv.y, dt)
            end
          end
          
          if options.isAttractedByMouse then
            v = getAttracted(v, mouseX, mouseY, dt)
          end
          
          v = moveObject(v, dt)
          v = speedLoss(v, dt)
          
          --Limite la vitesse si cette option est activée
          if options.isSpeedLimited then
            if v.spdX < -options.speedLimit then
              v.spdX = -options.speedLimit
            end
            if v.spdX > options.speedLimit then
              v.spdX = options.speedLimit
            end
            
            if v.spdY < -options.speedLimit then
              v.spdY = -options.speedLimit
            end
            if v.spdY > options.speedLimit then
              v.spdY = options.speedLimit
            end
          end
          
          --Définit une couleur selon la vitesse
          
        end
      end
    end
  end
end

function love.draw()
  for i, v in pairs(container) do
    lg.setColor(v.colors.r/255, v.colors.g/255, v.colors.b/255)
    lg.circle(v.drawType, v.x, v.y, v.rad)
  end
  
  --[[Menu]]--
  lg.setColor(0, 200/255, 0)
  lg.setFont(font)
  lg.print("Appuyez sur TAB pour afficher/cacher le menu", 10, 10)
  lg.print("Appuyez sur la barre espace pour afficher l'aide et les contrôles", 10, 30)
  
  if menu then
    lg.print("Nombre d'objets : "..#container, 10, 50)
    lg.circle("fill", 183, 58, 6)
    lg.print(" : "..counter[1], 186, 50)
    lg.circle("line", 253, 58, 6)
    lg.print(" : "..counter[2], 256, 50)
    lg.print(string.format("Force de la gravité : %d", gravityForceSpeed*100000).."\n"..
      string.format("Perte de vitesse : %d", speedLossForce*10000).."\n", 10, 70)
    lg.setFont(bigFont)
    lg.print("Options :", 10, 110)
    lg.setFont(font)
    local screenOption = ""
    if options.screenBorder == 0 then screenOption = "Passe à l'autre bout de la fenêtre"
    elseif options.screenBorder == 1 then screenOption = "Les objets rebondissent sur les bords de la fenêtre"
    elseif options.screenBorder == 2 then screenOption = "Sors de la fenêtre"
    end
    
    local massOption = ""
    if options.objectsMass == 0 then massOption = "Petit"
    elseif options.objectsMass == 1 then massOption = "Moyen"
    elseif options.objectsMass == 2 then massOption = "Grand"
    elseif options.objectsMass == 3 then massOption = "Enorme"
    elseif options.objectsMass == 4 then massOption = "Aléatoire"
    end
    lg.print("[1] Objets attirés entre eux : "..tostring(options.isAttractedByObject).."\n"..
    "[2] Objets attirés par la souris : "..tostring(options.isAttractedByMouse).."\n"..
    "[3] Vitesse des objets limitée : "..tostring(options.isSpeedLimited).."\n"..
    "[4] Bords de fenêtre : "..screenOption.."\n"..
    "[5] Taille des objets crées : "..massOption.."\n"
    , 10, 130)
  end
  
  if help.val then
    lg.setColor(200/255, 200/255, 200/255, 50/255)
    lg.rectangle("fill", wdow.wth - help.wth - 10, wdow.hgt - help.hgt - 10, help.wth, help.hgt)
    
    lg.setColor(0, 200/255, 0/255)
    lg.setFont(bigFont)
    lg.print("Aide :", wdow.wth - help.wth, wdow.hgt - help.hgt)
    lg.print("Touches :", wdow.wth - help.wth, wdow.hgt - help.hgt + 47)
    lg.setFont(font)
    lg.print("Changez les options en utilisant les touches numérotées", wdow.wth - help.wth, wdow.hgt - help.hgt + 17)
    
    lg.print("[Clic gauche] Crée un objet soumis à la gravité\n"..
      "[Clic droit] Crée un objet non soumis à la gravité\n"..
      "[Molette] change la force de la gravité\n"..
      "[Molette + CTRL] change la perte de vitesse\n"..
      "[Delete] supprime un objet non soumis à la gravité\n"..
      "[Backspace] supprime un objet soumis à la gravité\n"..
      "[P] Met la simulation sur pause\n"..
      "[F11] Pleine écran"
      --"[ESC] Quitte la simulation"
      , wdow.wth - help.wth, wdow.hgt - help.hgt + 67)
  end
  
  if pause then
    lg.setColor(255/255, 255/255, 255/255)
    lg.setFont(hugeFont)
    lg.print("PAUSE", wdow.wth/2 - hugeFont:getWidth("PAUSE")/2, wdow.hgt/2 - hugeFont:getHeight()/2)
    lg.setBackgroundColor(0, 0, 50/255)
  else
    lg.setBackgroundColor(20/255, 20/255, 70/255)
  end
end